﻿using UnityEngine;
using UnityEngine.UI;

public class ChangeColorImage : MonoBehaviour
{
    [SerializeField] private Image _imageBackground;    
    void Update()
    {        
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Debug.Log($"Click");
            _imageBackground.color = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
        }
    }
}
